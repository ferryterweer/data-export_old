package nl.haarlem.utils.dataexport.models;

import lombok.Data;

@Data
public class DataExportFile {

    private String mimeType;
    private String base64String;
    private String extension;

}
