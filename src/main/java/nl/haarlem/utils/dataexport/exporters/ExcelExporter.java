package nl.haarlem.utils.dataexport.exporters;

import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import nl.haarlem.utils.dataexport.models.DataExportFile;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;

import java.io.*;
import java.lang.reflect.Field;

import java.util.Base64;
import java.util.List;

@Component
@Log4j2
public class ExcelExporter {


    @SneakyThrows
    public DataExportFile createExcelFile(String sheetName, List<Object> records) throws IOException {
        if (records.size() == 0) {
            log.info("No rows found, Excel-file was not created.");
            return null;
        }

        Workbook wb = new XSSFWorkbook();
        Sheet sheet = wb.createSheet(sheetName);

        //Add field-names as first row
        Row row = sheet.createRow(0);
        var firstRecord = records.get(0);
        Field[] fields = firstRecord.getClass().getDeclaredFields();
        for (var i = 0; i < fields.length; i++) {
            fields[i].setAccessible(true);
            row.createCell(i).setCellValue(fields[i].getName());
        }

        //Add field values for each record
        var rowNum = 1;

        for (Object record : records) {
            fields = record.getClass().getDeclaredFields();
            row = sheet.createRow(rowNum);
            for (var i = 0; i < fields.length; i++) {
                fields[i].setAccessible(true);
                if (fields[i].get(record) == null) {
//                    row.createCell(i).setCellValue("NULL");
                    row.createCell(i).setCellValue(String.format("%s", fields[i].get(record)));
                } else if (fields[i].getType() == Integer.class) {
//                    row.createCell(i).setCellValue((int) fields[i].get(record));
                    row.createCell(i).setCellValue(String.format("%s", fields[i].get(record)));
                } else {
//                    row.createCell(i).setCellValue((String) fields[i].get(record));
                    row.createCell(i).setCellValue(String.format("%s", fields[i].get(record)));
                }
            }
            rowNum++;
        }

        ByteArrayOutputStream os = new ByteArrayOutputStream();

        try {
            wb.write(os);
            byte[] bytes = os.toByteArray();
            os.close();
            return new DataExportFile()
                    .setMimeType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                    .setExtension(".xlsx")
                    .setBase64String(Base64.getEncoder().encodeToString(bytes));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
