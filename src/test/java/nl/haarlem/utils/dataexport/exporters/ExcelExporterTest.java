package nl.haarlem.utils.dataexport.exporters;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ExcelExporterTest {

    ExcelExporter excelExporter = new ExcelExporter();


    @SneakyThrows
    @Test
    void createExcelFile() {

        //Assign
        List<Object> testObject = List.of(
                new Object() {
                    final String stringColumn = "A1";
                    final Integer integerColumn = 1;
                    final LocalDateTime localDateTimeColumn = LocalDateTime.now();
                },
                new Object() {
                    final String stringColumn = "B1";
                    final Integer integerColumn = 2;
                    final LocalDateTime localDateTimeColumn = LocalDateTime.now();
                }
        );

        //Act
        var result = excelExporter.createExcelFile("test", testObject);

        //Assert
        assertFalse(result.getBase64String().isEmpty());

    }
}
